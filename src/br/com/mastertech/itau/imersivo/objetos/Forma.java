package br.com.mastertech.itau.imersivo.objetos;

public abstract class Forma {
	
	public abstract double calculaArea();
	
	public abstract String getNome();
	
	@Override
	public String toString() {
		return "A área do " + getNome() + " é " + calculaArea();
	}
	
}
