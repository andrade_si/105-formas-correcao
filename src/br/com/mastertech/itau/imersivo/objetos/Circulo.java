package br.com.mastertech.itau.imersivo.objetos;

public class Circulo extends Forma {
	
	private double raio;

	public Circulo(double raio) {
		this.raio = raio;
	}
	
	@Override
	public String getNome() {
		return "Circulo";
	}
	
	@Override
	public double calculaArea() {
		return (Math.PI * Math.pow(raio, 2));
	}
	
	
}
